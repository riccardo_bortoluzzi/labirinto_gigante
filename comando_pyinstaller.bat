python -m pip install --upgrade pip
python -m pip install --upgrade pyinstaller 
python -m pip install --upgrade pyqt5

pyinstaller --distpath .\compilato_windows ^
            --noconfirm ^
			--clean ^
			--onefile ^
			--name Labirinto ^
			--hiddenimport os ^
			--hiddenimport typing ^
			--hiddenimport random ^
			--hiddenimport time ^
			--hiddenimport math ^
			--hiddenimport datetime ^
			--hiddenimport sys ^
			--hiddenimport traceback ^
			--hiddenimport PyQt5 ^
			--hiddenimport Costanti ^
			--hiddenimport Labirinto ^
			--hiddenimport GraficaLabirintoEffettiva ^
			--hiddenimport GraficaQtLabirinto ^
			--console ^
			--icon .\icona\screenshot_sfondo_bianco.ico ^
			--version-file .\version_file.txt ^
			--win-private-assemblies ^
			--win-no-prefer-redirects ^
			main_labirinto_grafica.py

pause