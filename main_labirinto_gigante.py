#script per gestire il main del labirinto

import math, sys

import Labirinto, Costanti

if __name__ == '__main__':
	dim_v = 20
	dim_o = 20
	lunghezza_minima_sol = 100
	profondita_ampliamento_percorso = math.inf
	#tipologia_ricerca = Costanti.labirinto_tipologia_ricerca_ferma_prima_muro
	tipologia_ricerca = Costanti.labirinto_tipologia_ricerca_non_fermare_prima_muro
	flag_inserisci_in_automatico_muro = True
	tempo_limite = 30

	#aggiungo il limite di ricorsione
	sys.setrecursionlimit(dim_v*dim_o)

	partenza = (0,1)
	arrivo = (dim_v-1, dim_o-1-1)

	nuovo_labirinto = Labirinto.Labirinto(dim_o=dim_o, 
										  dim_v=dim_v, 
										  partenza=partenza, 
										  arrivo=arrivo, 
										  lunghezza_minima_soluzione=lunghezza_minima_sol, 
										  tipologia_ricerca=tipologia_ricerca,
										  flag_inserisci_automaticamente_bordo=flag_inserisci_in_automatico_muro,
										  tempo_limite= tempo_limite
										)

	print('percorso banale:')
	nuovo_labirinto.stampa_labirinto(flag_soluzione=False)

	#mi calcolo l'espansione del percorso
	nuovo_labirinto.gestisci_processo_popolamento_matrice_numeri(profondita=profondita_ampliamento_percorso)
	print('\n\nSchema completo:')
	nuovo_labirinto.stampa_labirinto(flag_soluzione=False)

	print('\n\nSoluzione:')
	nuovo_labirinto.stampa_labirinto(flag_soluzione=True)
	