#script che contiene la classe per la grafica effettiva del labirinto

import math, datetime, sys, time
import traceback
from typing import List, Tuple
from PyQt5 import QtCore, QtGui, QtWidgets

import GraficaQtLabirinto, Labirinto, Costanti

class GraficaLabirintoEffettiva(QtWidgets.QMainWindow):
	#estende la classe di qt

	grafica_ui : GraficaQtLabirinto.Ui_GraficaQtLabirinto
	matrice_schema : List[List[int]]
	soluzione : List[Tuple[int, int]] 
	lista_celle_seguite : List[Tuple[int, int]]
	flag_mostra_soluzione : bool
	tempo_inizio : datetime.datetime #tempo di inizio del labirinto
	tempo_fine : datetime.datetime #tempo fine del labirinto

	def __init__(self) -> None:
		super().__init__()

		ui = GraficaQtLabirinto.Ui_GraficaQtLabirinto()
		ui.setupUi(self)

		self.grafica_ui = ui

		self.aggiorna_combobox_partenza_arrivo()

		#mi salvo in sessione i parametri a None
		self.matrice_schema = []
		self.soluzione = []
		self.lista_celle_seguite = []
		self.flag_mostra_soluzione = False


	#metto la procedura per aggiornare le partenze e gli arrivi

	def aggiorna_combobox_partenza_arrivo(self):
		#metodo per aggiornare le combobox di partenza e arrivo
		#mi ricavo dim_v e dim_o
		dim_v = self.grafica_ui.spinBox_numero_righe.value()
		dim_o = self.grafica_ui.spinBox_numero_colonne.value()
		#svuoto le combobox
		self.grafica_ui.comboBox_arrivo.clear()
		self.grafica_ui.comboBox_partenza.clear()
		#itero tra tutte le righe e colonne
		#metto come entrate e uscite solo quelle dei bordi
		lista_celle_escluse = [ (r,c) for r in (0, dim_v-1) for c in ( 0, dim_o-1 ) ]
		for r in range(dim_v):
			for c in range(dim_o):
				if (r,c) not in lista_celle_escluse:
					stringa_opzione = str( (r+1, c+1) )
					#le aggiungo alle combobox
					self.grafica_ui.comboBox_partenza.addItem(stringa_opzione, (r, c))
					self.grafica_ui.comboBox_arrivo.addItem(stringa_opzione, (r, c))
		#seleziono le opzioni
		self.grafica_ui.comboBox_partenza.setCurrentIndex(0)
		self.grafica_ui.comboBox_arrivo.setCurrentIndex(self.grafica_ui.comboBox_arrivo.count()-1)
		#aggiorno la lunghezza massima
		self.grafica_ui.spinBox_min_len_sol.setMaximum(int(((dim_v-2)*(dim_o-2))*2/3)) #metto i -2 per comprendere i muri ai lati

	#funzione per ridimensionare le celle della tabella
	def ridimensiona_celle_tabella(self):
		#metodo per ridimensionare le celle della tabella
		#recupero la dimensione dei q2uadrati
		dimensione_quadrati = self.grafica_ui.spinBox_dim_quadrati.value()
		#metto le dimensioni delle colonne
		for r in range(self.grafica_ui.tableWidget_labirinto.rowCount()):
			self.grafica_ui.tableWidget_labirinto.setRowHeight(r, dimensione_quadrati)
		for c in range(self.grafica_ui.tableWidget_labirinto.columnCount()):
			self.grafica_ui.tableWidget_labirinto.setColumnWidth(c, dimensione_quadrati)
		#adesso provo a ridimensionare il widget
		#self.grafica_ui.widget_tabella.setFixedHeight(self.grafica_ui.tableWidget_labirinto.height())
		#self.grafica_ui.widget_tabella.setFixedWidth(self.grafica_ui.tableWidget_labirinto.width())

		#self.grafica_ui.scrollArea_tabella.setFixedHeight(self.grafica_ui.tableWidget_labirinto.height())
		#self.grafica_ui.scrollArea_tabella.setFixedWidth(self.grafica_ui.tableWidget_labirinto.width())

	#funzione per creare una nuova partita
	def crea_nuova_partita(self):
		#creo la nuova partita in base 
		#mi recupero dim v e dim o
		dim_v = self.grafica_ui.spinBox_numero_righe.value()
		dim_o = self.grafica_ui.spinBox_numero_colonne.value()
		#mi recupero partenza e arrivo
		partenza = self.grafica_ui.comboBox_partenza.currentData()
		arrivo = self.grafica_ui.comboBox_arrivo.currentData()
		#mi recupero profondita
		profondita = self.grafica_ui.spinBox_profondita.value()
		if profondita == 0:
			#metto l'infinito
			profondita = math.inf
		#mi recupero la tipologia di espansione
		#uso lo stesso indice delle costanti
		tipologia_ricerca = self.grafica_ui.comboBox_modalita_ampliamento.currentIndex()
		#mi ricavo la lunghezza della soluzione minima
		lunghezza_soluzione = self.grafica_ui.spinBox_min_len_sol.value()
		#mi ricavo il tempo limite
		tempo_limite = self.grafica_ui.spinBox_tempo_limite.value()
		#pulisco la tabella
		self.grafica_ui.tableWidget_labirinto.setRowCount(0)
		self.grafica_ui.tableWidget_labirinto.setColumnCount(0)
		#aggiungo il limite di ricorsione
		#aggiungo il limite di ricorsione
		sys.setrecursionlimit(dim_v*dim_o)
		#mi creo l'indovinello
		try:
			nuovo_indovinello = Labirinto.Labirinto(dim_v=dim_v,
													dim_o=dim_o,
													partenza=partenza,
													arrivo=arrivo,
													lunghezza_minima_soluzione=lunghezza_soluzione,
													tipologia_ricerca=tipologia_ricerca,
													flag_inserisci_automaticamente_bordo=True,
													tempo_limite=tempo_limite)
			#lo amplio
			nuovo_indovinello.gestisci_processo_popolamento_matrice_numeri(profondita=profondita)
			flag_generazione_terminata = True
		except Exception as e:
			nuovo_indovinello = None
			flag_generazione_terminata = False
			etype, value, tb = sys.exc_info()
			stringa_errore = ''.join(traceback.format_exception(etype, value, tb))
			stringa_da_stampare = time.strftime('%d/%m/%Y %H:%M:%S') + ' - ' + str(etype) + '\n' + stringa_errore +'\n\n'
			print(stringa_da_stampare)
			#in l'utente che non e stato possibile completare lo schema
			msg = QtWidgets.QMessageBox()
			msg.setIcon(QtWidgets.QMessageBox.Critical)
			msg.setText(f"Non è possibile generare l'indovinello con i parametri assegnati")
			msg.setWindowTitle("Errore")
			msg.exec_()
		if flag_generazione_terminata and (nuovo_indovinello is not None):
			#me lo salvo nei rispettivi parametri
			self.matrice_schema = nuovo_indovinello.matrice_numeri
			self.soluzione = nuovo_indovinello.soluzione
			#inserisco la prima cella nella lista di quelle seguite
			self.lista_celle_seguite = [ self.soluzione[0] ]
			#una volta creato l'indovinello popolo la tabella
			self.grafica_ui.tableWidget_labirinto.setRowCount(dim_v)
			self.grafica_ui.tableWidget_labirinto.setColumnCount(dim_o)
			#metto bianca o nera in base al muro
			for r in range(dim_v):
				for c in range(dim_o):
					nuovo_item = QtWidgets.QTableWidgetItem()
					#verifico se e un muro, se e la partenza, se e l'arrivo, altrimenti bianca
					if self.matrice_schema[r][c] in (Costanti.labirinto_numero_muro, Costanti.labirinto_numero_non_analizzato):
						#metto la cella nera
						nuovo_item.setBackground(QtGui.QColor(Costanti.grafica_colore_muro))
						#nuovo_item.setText(Costanti.labirinto_carattere_muro)
					elif (r,c) == self.soluzione[0]:
						#e la partenza la coloro di verde
						nuovo_item.setBackground(QtGui.QColor(Costanti.grafica_colore_partenza))
					elif (r,c) == self.soluzione[-1]:
						#e l'arrivo lo coloro di rosso
						nuovo_item.setBackground(QtGui.QColor(Costanti.grafica_colore_arrivo))
					else:
						#e di passaggio la coloro di bianco
						nuovo_item.setBackground(QtGui.QColor(Costanti.grafica_colore_vuoto))
					#aggiungo l'elemento alla tabella
					self.grafica_ui.tableWidget_labirinto.setItem(r, c, nuovo_item)
			#aggiorno le dimensioni della tabella
			self.ridimensiona_celle_tabella()
			#adesso metto il focus sulla tabella 
			self.grafica_ui.tableWidget_labirinto.setFocus()
			#seleziono la casella di entrata
			(r_partenza, c_partenza) = self.soluzione[0]
			#self.grafica_ui.tableWidget_labirinto.item(r_partenza, c_partenza).setSelected(True)
			#self.grafica_ui.tableWidget_labirinto.setSelection(QtCore.QRect(r_partenza, c_partenza, 1, 1) )
			#self.grafica_ui.tableWidget_labirinto.item(r_partenza, c_partenza).setSe
			#self.grafica_ui.tableWidget_labirinto.setRangeSelected(QtWidgets.QTableWidgetSelectionRange(r_partenza, c_partenza, r_partenza, c_partenza), True)
			self.grafica_ui.tableWidget_labirinto.setCurrentCell(r_partenza, c_partenza)
			#metto il flag per non mostrare la soluzione
			self.flag_mostra_soluzione = False
			#popolo il tempo di inizio
			self.tempo_inizio = datetime.datetime.now()
			self.tempo_fine = self.tempo_inizio
		

	def cella_cambiata(self) -> None:
		#metodo per gestire la selezione di un'altra cella
		#mi ricavo riga e colonna
		riga = self.grafica_ui.tableWidget_labirinto.currentRow()
		col = self.grafica_ui.tableWidget_labirinto.currentColumn()
		print(f'cella_cambiata({riga}, {col})')
		#per prima cosa verifico se e adiacente all'ultima cella selezionata
		(r_ultima, c_ultima) = self.lista_celle_seguite[-1]
		if (riga, col) in self.lista_celle_seguite:
			#allora ho deciso di tornare indietro, sbianco tutte quelle successive e tronco la lista delle celle visualizzate
			indice_click = self.lista_celle_seguite.index((riga, col))
			for (rr, cc) in self.lista_celle_seguite[indice_click+1:]:
				if (rr, cc) not in ( self.soluzione[0], self.soluzione[-1] ):
					#verifico se devo mostrarlo come soluzione
					if (self.flag_mostra_soluzione) and ((rr, cc) in self.soluzione):
						#coloro di arancione come il colore della soluzione
						self.grafica_ui.tableWidget_labirinto.item(rr, cc).setBackground(QtGui.QColor(Costanti.grafica_colore_soluzione))
					else:
						#la sbianco se non e partenza o arrivo
						self.grafica_ui.tableWidget_labirinto.item(rr, cc).setBackground(QtGui.QColor(Costanti.grafica_colore_vuoto))
			#svuoto la lista delle celle
			self.lista_celle_seguite = self.lista_celle_seguite[:indice_click+1]
			#vado a selezionare quella cliccata
			self.grafica_ui.tableWidget_labirinto.setCurrentCell(riga, col)
		elif ( abs(riga-r_ultima) + abs(col-c_ultima) ) ==1:
			#vedo se posso selezionarla
			if self.matrice_schema[riga][col] == Costanti.labirinto_numero_passaggio:
				#allora e una cella di passaggio, posso andarci
				#la coloro solo se non e partenza o arrivo
				if (riga, col) not in ( self.soluzione[0], self.soluzione[-1] ):
					#la coloro di giallo
					self.grafica_ui.tableWidget_labirinto.item(riga, col).setBackground(QtGui.QColor(Costanti.grafica_colore_passaggio))
				#vado a selezionare quella casella
				self.grafica_ui.tableWidget_labirinto.setCurrentCell(riga, col)
				#la aggiungo alla lista
				self.lista_celle_seguite.append( (riga, col) )
				#verifico se sono arrivato alla fine
				if ( (riga, col) == self.soluzione[-1] ):
					#verifico se ho gia popolato il tempo di fine
					if self.tempo_fine == self.tempo_inizio:
						self.tempo_fine = datetime.datetime.now()
					#mi calcolo la differenza di tempo
					delta_tempo = self.tempo_fine - self.tempo_inizio
					delta_minuti = round(delta_tempo.total_seconds()//60)
					delta_secondi = round(delta_tempo.total_seconds()%60)
					#mostro un messaggio all'utente informandolo che e arrivato alla soluzione
					msg = QtWidgets.QMessageBox()
					msg.setIcon(QtWidgets.QMessageBox.Information)
					msg.setText(f"Complimenti hai risolto il labirinto in {delta_minuti}:{delta_secondi}")
					msg.setWindowTitle("Hai Vinto!!")
					msg.exec_()
			else:
				#e una cella muro, non faccio niente e torno alla precednete
				self.grafica_ui.tableWidget_labirinto.setCurrentCell(r_ultima, c_ultima)
		else:
			#sono andato fuori campoi, ritorno sull'ultima cella selezionata
			self.grafica_ui.tableWidget_labirinto.setCurrentCell(r_ultima, c_ultima)

	#metodo per mostrare la soluzione o nasconderla
	def mostra_nascondi_soluzione(self):
		#e fattibile solo se la soluzione han una lunghezza maggiore di 1
		print('mostra_nascondi_soluzione')
		if len(self.soluzione) >1:
			#mi caclolo il colore da applicare in base al flag della soluzione
			if self.flag_mostra_soluzione:
				#sto mostrando la soluzione -> devo sbiancare
				colore = Costanti.grafica_colore_vuoto
			else:
				#non sto mostrando la soluzione -> devo mostrarla
				colore = Costanti.grafica_colore_soluzione
			#itero per tutte le celle della soluzione dalla seconda alla penultima
			for (r, c) in self.soluzione[1:-1]:
				#se non e presente nel percorso attuale la coloro
				if (r,c) not in self.lista_celle_seguite:
					self.grafica_ui.tableWidget_labirinto.item(r, c).setBackground(QtGui.QColor(colore))
			#al termine cambio il flag per la soluzione
			self.flag_mostra_soluzione = not(self.flag_mostra_soluzione)
			#al termine torno con il focus nella tabella
			self.grafica_ui.tableWidget_labirinto.setFocus()
		

	