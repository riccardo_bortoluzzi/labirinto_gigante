#script contenente le costanti per il gioco del labirinto gigante, creato per sperimentare la capacità del computer

import os

cartella_attuale = os.path.abspath('.')

labirinto_numero_non_analizzato = -1
labirinto_numero_muro = 0
labirinto_numero_passaggio = 1

'''
#metto i flag per distinguere come sviluppare la ricerca di percorsi alternativi
labirinto_flag_ricerca_espansione_solo_1 = 0 #ogni volta che ricerca un percorso alternativo espande solo 1 elemento
labirinto_flag_ricerca_termina_prima_invalida_adiacenti_non_filtrati = 1 #quando espande non va a vedere se quelle adiacenti sono gia state classificate, se ne trova una classificata termina
labirinto_flag_ricerca_termina_prima_invalida_adiacenti_filtrati = 2 #quando espande va a ricercare solo tra quele ancora libere, si ferma appena un percorso trova una cella che andrebbe colorata con un muro
labirinto_flag_ricerca_non_termina = 3 #quando trova una strada che non va bene prosegue verso un altra direzione, termina quano non si puo proprio piu continuare ad espandere il percorso, potrebbe creare spirali


DEPRECATI QUESTI FLAG PERCHE VADO AD ESPANDERE COMUNQUE TUTTO, METTO SOLO QUELLO PER VERIFICARE SE DEVO ESPANDERMI SOLO DI 1 OGNI VOLTA

'''

labirinto_tipologia_ricerca_non_fermare_prima_muro = 0 #nel momento della ricerca di nuove celle bianche non si ferma mai ma prova tutte le possibili strade da quella casella
labirinto_tipologia_ricerca_ferma_prima_muro = 1 #nel momento della ricerca, appena trova la prima a muro si ferma #SORPRENDENTEMENTE DALLA GRAFICA QUESTA È PIÙ DIFFICILE


#caratteri per stampare labirinto
labirinto_carattere_muro = '⬛'
labirinto_carattere_passaggio = '⬜'
labirinto_carattere_soluzione = '🟨'#'🔺'

labirinto_matrice_muri_esterni_numero_muro = 0
labirinto_matrice_muri_esterni_numero_libero = 1
labirinto_matrice_muri_esterni_numero_soluzione = 2

#costanti per la grafica
grafica_dimensione_quadrato = 10

grafica_colore_partenza = 'green'
grafica_colore_arrivo = 'red'
grafica_colore_vuoto = 'white'
grafica_colore_muro = 'black'
grafica_colore_passaggio = 'cyan'
grafica_colore_soluzione = 'orange'