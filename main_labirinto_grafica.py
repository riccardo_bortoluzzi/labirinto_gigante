#!.venv/bin/python3

#script per il main del labirinto della grafica

import GraficaLabirintoEffettiva
from PyQt5 import QtCore, QtGui, QtWidgets

if __name__ == '__main__':
	import sys
	app = QtWidgets.QApplication(sys.argv)

	grafica = GraficaLabirintoEffettiva.GraficaLabirintoEffettiva()
	grafica.show()

	sys.exit(app.exec_())
