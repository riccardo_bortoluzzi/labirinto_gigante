#script che contiene la classe labirinto

from typing import List, Tuple, Union
import random, time

import Costanti

#mi inserisco alcune costanti per il labirinto

class Labirinto:
	matrice_numeri : List[List[int]]
	soluzione : List[Tuple[int, int]]
	dim_v : int
	dim_o : int
	flag_inserisci_automaticamente_bordo : bool #flag che indica se inserire in automatico il bordo all'interno dello schema oppure da aggiungere in un secondo momento

	#vari flag che mi servono per la generazione dello schema
	#flag_scelta_casuale_adiacenti_non_filtrati : bool #flag che indica se quando riempire il percorso prendo casualmente solo 1 delle possibili direzioni, se gia analizzata si ferma, altrimenti procede con il successivo flag
	#flag_scelta_casuale_adiacenti_filtrati : bool #flag che scatta se il precedente non viene verificato: quando riempie il labirinto cerca tra quelli non analizzati adiacenti, pero si ferma alla prima prova, non va a ricorsione se il primo che analizza non si può continuare, altrimenti cerca di espandere i percorsi alternativi il più possibile, potrebbe generare spirali

	tipologia_ricerca : int

	tempo_inizio : float #indicano il tempoi di inizio di calcolo e il tempo limite
	tempo_limite : float

	def __init__(self, dim_v:int, dim_o:int, partenza : Tuple[int, int], arrivo:Tuple[int, int], lunghezza_minima_soluzione:int, tipologia_ricerca:int, flag_inserisci_automaticamente_bordo : bool, tempo_limite:float) -> None:
		#metodo costruttore che valorizza solo i parametri
		self.dim_v = dim_v
		self.dim_o = dim_o
		self.tipologia_ricerca = tipologia_ricerca
		self.matrice_numeri = [ [ Costanti.labirinto_numero_non_analizzato for c in range(self.dim_o) ] for r in range(self.dim_v) ]
		self.flag_inserisci_automaticamente_bordo = flag_inserisci_automaticamente_bordo
		self.tempo_limite = tempo_limite
		#creeo la soluzione con l'apposita procedura
		self.gestisci_processo_creazione_soluzione(lunghezza_minima=lunghezza_minima_soluzione, partenza=partenza, arrivo=arrivo)

	########################################################################################################################################################################################
	#metodi generali

	def calcola_adiacenti(self, r:int, c:int) -> List[Tuple[int, int]]:
		#metodo per calcolare le celle adiacenti
		lista_celle_adiacenti = []
		#METTO I < E -1 PER EVITARE DI PRENDERE CELLE SUL BORDO OLTRE A QUELLE DI ENTRATA E CHIUSURA
		if self.flag_inserisci_automaticamente_bordo:
			for dr in (-1, 1):
				if 0 < r+dr < self.dim_v -1:
					lista_celle_adiacenti.append( (r+dr, c) )
			for dc in (-1, 1):
				if 0 < c+dc < self.dim_o -1:
					lista_celle_adiacenti.append( (r, c +dc) )
		else:
			#il bordo lo inserisco dopo
			for dr in (-1, 1):
				if 0 <= r+dr < self.dim_v:
					lista_celle_adiacenti.append( (r+dr, c) )
			for dc in (-1, 1):
				if 0 <= c+dc < self.dim_o:
					lista_celle_adiacenti.append( (r, c +dc) )
		return lista_celle_adiacenti

	################################################################################################################################################################################################
	#metodi per stampare il labirinto

	def stampa_labirinto(self, flag_soluzione:bool) -> None:
		#metodo che stampa il labirinto riga pewr riga
		#mi calcolo la matrice con i muri esterni
		matrice_muri_esterni = self.crea_matrice_labirinto_muri_esterni(flag_soluzione=flag_soluzione)
		#vado a crearmi ogni riga
		for riga in matrice_muri_esterni:
			stringa_riga = ''
			for elemento in riga:
				#faccio la distinzione in base all'elemento
				if elemento == Costanti.labirinto_matrice_muri_esterni_numero_muro:
					stringa_riga += Costanti.labirinto_carattere_muro
				elif elemento == Costanti.labirinto_matrice_muri_esterni_numero_libero:
					stringa_riga += Costanti.labirinto_carattere_passaggio
				elif elemento == Costanti.labirinto_matrice_muri_esterni_numero_soluzione:
					stringa_riga += Costanti.labirinto_carattere_soluzione
			#stampo la riga
			print(stringa_riga)
		'''
		#per prima cosa mi calcolo i caratteri
		if flag_soluzione:
			#sto mostrando la soluzione
			carattere_passaggio_soluzione = Costanti.labirinto_carattere_soluzione
		else:
			#non sto mostrando la soluzione
			carattere_passaggio_soluzione = Costanti.labirinto_carattere_passaggio
		#metto una cornice di muri tranne per entrata e uscita
		str_prima_riga = Costanti.labirinto_carattere_muro
		colonna_partenza = self.soluzione[0][1]
		for o in range(self.dim_o):
			#vedo se e la colonna di partenza
			if o == colonna_partenza:
				#metto il vuoto
				str_prima_riga += carattere_passaggio_soluzione
			else:
				#non entro dfa quella parte
				str_prima_riga += Costanti.labirinto_carattere_muro
		#al termine metto un ulteriore muro per l'ultima colonna
		str_prima_riga += Costanti.labirinto_carattere_muro
		print(str_prima_riga)
		#adesso metto tutte le altre righe
		for r in range(self.dim_v):
			stringa_riga = Costanti.labirinto_carattere_muro
			#itero tra tutte le colonne
			for c in range(self.dim_o):
				#verifico se e muro o bianco o soluzione
				if self.matrice_numeri[r][c] in (Costanti.labirinto_numero_muro, Costanti.labirinto_numero_non_analizzato) :
					stringa_riga += Costanti.labirinto_carattere_muro
				elif (r,c) in self.soluzione:
					#metto il carattere della soluzione
					stringa_riga += carattere_passaggio_soluzione
				else:
					#e una casella vuota
					stringa_riga += Costanti.labirinto_carattere_passaggio
			#adesso metto l'ultima colonna
			stringa_riga += Costanti.labirinto_carattere_muro
			#stampo la stringa
			print(stringa_riga)
		#adesso devo mettere l'ultima riga
		str_ultima_riga = Costanti.labirinto_carattere_muro
		colonna_arrivo = self.soluzione[-1][1]
		for o in range(self.dim_o):
			#vedo se e la colonna di partenza
			if o == colonna_arrivo:
				#metto il vuoto
				str_ultima_riga += carattere_passaggio_soluzione
			else:
				#non entro dfa quella parte
				str_ultima_riga += Costanti.labirinto_carattere_muro
		#al termine metto un ulteriore muro per l'ultima colonna
		str_ultima_riga += Costanti.labirinto_carattere_muro
		print(str_ultima_riga)
		'''

	def crea_matrice_labirinto_muri_esterni(self, flag_soluzione:bool) -> List[List[int]]:
		#metodo che prende la matrice dei numeri e ci mette i muri al bordo, ritorna la nuova matrice
		#mi calcolo il numero relativo alla soluzione
		if flag_soluzione:
			#sto mostrando la soluzione
			numero_casella_soluzione = Costanti.labirinto_matrice_muri_esterni_numero_soluzione
		else:
			#non sto mostrando la soluzione
			numero_casella_soluzione = Costanti.labirinto_matrice_muri_esterni_numero_libero
		matrice_muri_esterni = []
		#metto la prima riga di soli muri se serve
		if not(self.flag_inserisci_automaticamente_bordo):
			matrice_muri_esterni.append( [ Costanti.labirinto_matrice_muri_esterni_numero_muro for c in range(self.dim_o +2) ] )
		#adesso itero tra tutte le righe
		for r in range(self.dim_v):
			lista_riga = []
			#ci metto la prima che e un muro se serve
			if not(self.flag_inserisci_automaticamente_bordo):
				lista_riga.append(Costanti.labirinto_matrice_muri_esterni_numero_muro)
			for c in range(self.dim_o):
				#verifico se e muro o bianco o soluzione
				if self.matrice_numeri[r][c] in (Costanti.labirinto_numero_muro, Costanti.labirinto_numero_non_analizzato) :
					lista_riga.append(Costanti.labirinto_matrice_muri_esterni_numero_muro)
				elif (r,c) in self.soluzione:
					#metto il numero della soluzione
					lista_riga.append(numero_casella_soluzione)
				else:
					#e una casella vuota
					lista_riga.append(Costanti.labirinto_matrice_muri_esterni_numero_libero)
			#adesso metto l'ultima colonna se server
			if not(self.flag_inserisci_automaticamente_bordo):
				lista_riga.append(Costanti.labirinto_matrice_muri_esterni_numero_muro)
			#inserisco la riga
			matrice_muri_esterni.append(lista_riga[:])
		#adesso inserisco l'ultima riga se serve
		if not(self.flag_inserisci_automaticamente_bordo):
			matrice_muri_esterni.append( [ Costanti.labirinto_matrice_muri_esterni_numero_muro for c in range(self.dim_o +2) ] )
		#adesso vado a mettere l'entrata e l'uscita
		(r_partenza, c_partenza) = self.soluzione[0]
		(r_arrivo, c_arrivo) = self.soluzione[-1]
		if not(self.flag_inserisci_automaticamente_bordo):
			if r_partenza == 0:
				#sono sulla prima riga
				matrice_muri_esterni[0][c_partenza+1] = numero_casella_soluzione
			elif c_partenza == 0:
				#sono sulla prima colonna
				matrice_muri_esterni[r_partenza +1][0] = numero_casella_soluzione
			#faccio la stessa cosa con l'arriuvo
			if r_arrivo == (self.dim_v-1):
				#sono sull'ultima
				matrice_muri_esterni[-1][c_arrivo+1] = numero_casella_soluzione
			elif c_arrivo == (self.dim_o-1):
				#sono sull'ultima colonna
				matrice_muri_esterni[r_arrivo +1][-1] = numero_casella_soluzione
		#adesso posso ritornare la matrice
		return matrice_muri_esterni


	#########################################################################################################################################################################################
	#metodi per creare la soluzione


	def crea_soluzione_banale_lineare(self, partenza : Tuple[int, int], arrivo:Tuple[int, int]) -> None:
		#metodo per c reare la solzuione banale composta dalla strada piu breve per raggiungere dalla partenza l'arrivo
		lista_celle_soluzione_banale = [partenza]
		#adesso itero fino a quando la colonna non e la stessa dell'arrivo
		riga_spostamento = partenza[0]
		colonna_attuale = partenza[1]
		while colonna_attuale != arrivo[1]:
			#mi calcolo la nuova cella
			#mi calcolo la direzione
			delta_col = arrivo[1] - colonna_attuale
			direzione = int( delta_col/abs(delta_col) )
			colonna_attuale += direzione
			lista_celle_soluzione_banale.append((riga_spostamento, colonna_attuale))
		#quando arrivo alla colonna di arrivo, adesso cerco di scendere sulla riga
		(riga_attuale, colonna_spostamento) = lista_celle_soluzione_banale[-1]
		while riga_attuale != arrivo[0]:
			#mi calcolo la nuova cella
			#mi calcolo la direzione
			delta_riga = arrivo[0] - riga_attuale
			direzione = int( delta_riga/abs(delta_riga) )
			riga_attuale += direzione
			lista_celle_soluzione_banale.append((riga_attuale, colonna_spostamento))
		#al termine salvo la soluzione nella rispettiva lista
		self.soluzione = lista_celle_soluzione_banale

	def crea_soluzione_banale_scala(self, partenza : Tuple[int, int], arrivo:Tuple[int, int]) -> None:
		#metodo per creare la soluzione banale casualmente con una delle strade di lunghezza minore
		lista_celle_soluzione_banale = [partenza]
		#mi calcolo la lista delle celle impossibili, quelle del bordo se devo escluderle
		lista_celle_da_escludere = []
		if self.flag_inserisci_automaticamente_bordo:
			lista_celle_da_escludere = [ (r, c) for r in range(self.dim_v) for c in range(self.dim_o) if ( (r in (0, self.dim_v-1)) or (c in (0, self.dim_o-1)) )  ] 
		while lista_celle_soluzione_banale[-1] != arrivo:
			#mi calcolo le successive
			(r_ultima, c_ultima) = lista_celle_soluzione_banale[-1]
			#mi calcolo i delta
			delta_riga = arrivo[0] - r_ultima
			delta_col = arrivo[1] - c_ultima
			#mi calcolo la lista delle successive possibili
			lista_successive_possibili = []
			if delta_col != 0:
				#allora mi posso spostare sulle colonne
				direzione_col = int(delta_col / abs(delta_col))
				nuova_col = c_ultima + direzione_col
				#devo verificare se e nel bordo o non mi serve verificarlo
				if ( (r_ultima, nuova_col) == arrivo ) or ( (r_ultima, nuova_col) not in lista_celle_da_escludere ):
					lista_successive_possibili.append((r_ultima, nuova_col))
			if delta_riga != 0:
				#allora mi posso spostare sulle righe
				direzione_riga = int(delta_riga / abs(delta_riga))
				nuova_riga = r_ultima + direzione_riga
				#devo verificare se e nel bordo o non mi serve verificarlo
				if ( (nuova_riga, c_ultima) == arrivo ) or ( (nuova_riga, c_ultima) not in lista_celle_da_escludere ):
					lista_successive_possibili.append((nuova_riga, c_ultima))
			#ne prendo una e la aggiungo alla lista
			lista_celle_soluzione_banale.append(random.choice(lista_successive_possibili))
		#al termine salvo la soluzione nella rispettiva lista
		self.soluzione = lista_celle_soluzione_banale

	def inserisci_strada_alternativa(self, lista_percorso:List[Tuple[int, int]]) -> bool:
		#metodo che cerca una strada alternativa per la soluzione, strada che deve essere piu lunga, ritorna True se ha trovato una strada piu lunga, altrimenti ritorna False
		#per prima cosa verifico se ho superato il tempo limite
		if (time.time() - self.tempo_inizio) > self.tempo_limite:
			#dico che non ho trovato la soluzione
			return False
		#va a fare direttamente side effect sulla soluzione se ne trova una piu lunga
		#recupero l'ultima casella
		(ultima_r, ultima_c) = lista_percorso[-1]
		#se appartiene alla soluzione valuto se cerco di allungare il percorso oppure e il primo caso e devo continuare
		if (ultima_r, ultima_c) in self.soluzione:
			if len(lista_percorso) == 1:
				#sono al primo caso devo continuare
				#prendo le celle che ancora non appartengono alla soluzione
				celle_disponibili = [i for i in self.calcola_adiacenti(r=ultima_r, c=ultima_c) if i not in self.soluzione]
				random.shuffle(celle_disponibili)
				#adesso itero tra tutte le celle e proco a continuare
				for cella_successiva in celle_disponibili:
					if (self.inserisci_strada_alternativa(lista_percorso=lista_percorso + [cella_successiva] ) == True ):
						#ho trovato una strada alternativa
						return True
				#se al termine del ciclo non ha trovato una strada piu lunga allora ritorno false per indicvare che non e stata trovata
				return False
			else:
				#verifico se posso allungare il percorso
				#so che la prima e l'ultima appartengono al percorso soluzione, mi calcolo i rispettivi indici
				indice_prima = self.soluzione.index(lista_percorso[0])
				indice_ultima = self.soluzione.index(lista_percorso[-1])
				#mi calcolo minimo indice e massimo indice per capire quanti elementi ci sono all'interno
				(minimo_indice, massimo_indice) = sorted( [indice_prima, indice_ultima] )
				#mi calcolo quanti elementi ci sono tra i due
				elementi_in_soluzione = massimo_indice - minimo_indice + 1 #metto il +1 perche per esempio minimo indice=2, massimo indice=4, tra i 2 ci sono 3 elementi: 4-2+1
				if elementi_in_soluzione == 2:
					#non posso continuare perche sono 2 celle adiacenti e rischio di fare quadrati
					return False
				elif len(lista_percorso) > elementi_in_soluzione:
					#allora posso inserire il percorso nella soluzione
					#devo distinguere se il percorso in input va in quale ordine
					if minimo_indice == indice_prima:
						#la lista e gia ordinata
						self.soluzione = self.soluzione[:minimo_indice] + lista_percorso + self.soluzione[massimo_indice+1:] #metto il +1 perche l'ultima casella e gia dentro il percorso
					else:
						#devo invertire il percorso in input
						self.soluzione = self.soluzione[:minimo_indice] + lista_percorso[::-1] + self.soluzione[massimo_indice+1:] #metto il +1 perche l'ultima casella e gia dentro il percorso
					#dico che ho trovato un percorso piu lungo
					return True
				else:
					#non posso inserire il percorso nella soluzione
					return False
		else:
			#l'ultima casella non e dentro la soluzione
			#verifico se e adiacente a una della soluzione
			celle_adiacenti_in_solzuione = [ i for i in self.calcola_adiacenti(r=ultima_r, c=ultima_c) if i in self.soluzione ]
			#se la lunghezza e maggiore di uno questa strada non porta nessuna soluzione
			if len(celle_adiacenti_in_solzuione) >1:
				return False
			elif (len(celle_adiacenti_in_solzuione) == 1) and (len(lista_percorso) >2):
				#allora sono arrivato a una soluzione e provo a chiamare la ricorsione aggiungendo la cella del percorso
				return self.inserisci_strada_alternativa(lista_percorso=lista_percorso + [ celle_adiacenti_in_solzuione[0] ])
			else:
				#o sono al secondo step oppure quella adiacente non appartiene alla soluzione -> devo sempre iterare tra quelle che non sono nella soluzione
				lista_celle_da_escludere = self.soluzione + lista_percorso
				celle_adiacenti_disponibili_step_1 = [ i for i in self.calcola_adiacenti(r=ultima_r, c=ultima_c) if i not in lista_celle_da_escludere]
				#adesso itero e tolgo tutte quelle adiacenti al percorso appena calcolato
				celle_adiacenti_disponibili_step_2 = []
				for (r_next, c_next) in celle_adiacenti_disponibili_step_1:
					#mi calcolo le celle adiacenti che sono gia nel percorso attuale
					celle_adiacenti_candidata_gia_percorse = [ i for i in self.calcola_adiacenti(r=r_next, c=c_next) if i in lista_percorso[:-1] ]
					if len(celle_adiacenti_candidata_gia_percorse) == 0:
						#allora va bene e la aggiungo alla lista
						celle_adiacenti_disponibili_step_2.append( (r_next, c_next) )
				#a questo punto mescolo le celle e itero tra di esse
				random.shuffle(celle_adiacenti_disponibili_step_2)
				for cella_successiva in celle_adiacenti_disponibili_step_2:
					if (self.inserisci_strada_alternativa(lista_percorso=lista_percorso + [cella_successiva] ) == True ):
						#ho trovato una strada alternativa
						return True
				#se al termine del ciclo non ha trovato una strada piu lunga allora ritorno false per indicvare che non e stata trovata
				return False

	def amplia_soluzione_lunghezza_prefissata(self, minima_lunghezza : int) -> bool:
		#metodo che chiama la funzione per modificare la soluzione fino a quando non raggiunge la lunghezza prefissata
		#ritorno True se ha trovato una soluzione nel tempo prestabgilito, False altrimenti
		#mi calcolo anche il tempo per evitare di procedere troppo oltre con il tempo
		self.tempo_inizio = time.time()
		while len(self.soluzione) < minima_lunghezza:
			#mi prendo la soluzione che mescolo e parto da ciascuna cella
			if self.flag_inserisci_automaticamente_bordo:
				copia_soluzione = [ (r,c) for (r,c) in self.soluzione if ( (r not in (0, self.dim_v-1)) and ( c not in (0, self.dim_o-1) ) ) ]
			else:	
				copia_soluzione = self.soluzione[:]
			random.shuffle(copia_soluzione)
			#metto un flag per vedere se trovo una soluzione
			flag_trovata = False
			for cella_partenza in copia_soluzione:
				flag_trovata = self.inserisci_strada_alternativa(lista_percorso=[cella_partenza])
				#se la trovata esco dal ciclo
				if flag_trovata:
					break
				#verifico se sono oltre il tempo limite
				if (time.time() - self.tempo_inizio) > self.tempo_limite:
					#dico che non ho trovato la soluzione
					return False
			#al termine del ciclo verifico se l'ho trovata
			if not(flag_trovata):
				raise Exception('Impossibile trovare una soluzione della lunghezza richiesta')
		#al termine di tutto dico che ho trovato la soluzione
		return True
		

	def gestisci_processo_creazione_soluzione(self, lunghezza_minima: int, partenza : Tuple[int, int], arrivo:Tuple[int, int]) -> None:
		#metodo per gestire il processo di creazione della soluzione
		flag_soluzione_trovata = False
		while not(flag_soluzione_trovata):
			print('nuovo calcolo soluzione')
			#mi calcolo la soluzione e verifico di non superare il limite
			#self.crea_soluzione_banale_lineare(partenza=partenza, arrivo=arrivo) #QUESTA NON RISPETTA IL MURO ESTERNO AUTO CALCOLATO
			self.crea_soluzione_banale_scala(partenza=partenza, arrivo=arrivo)
			flag_soluzione_trovata = self.amplia_soluzione_lunghezza_prefissata(minima_lunghezza=lunghezza_minima)
		#verifico se devo mettere gia il bordo
		if self.flag_inserisci_automaticamente_bordo:
			celle_bordo = [ (r, c) for r in range(self.dim_v) for c in range(self.dim_o) if ( (r in (0, self.dim_v-1)) or (c in (0, self.dim_o-1)) ) ]
			for (r,c) in celle_bordo:
				self.matrice_numeri[r][c] = Costanti.labirinto_numero_muro
		#appena ho terminato di trovare la soluzione la inserisco nella matrice
		for (r,c) in self.soluzione:
			self.matrice_numeri[r][c] = Costanti.labirinto_numero_passaggio
		

	################################################################################################################################################################################################
	#metodi per ampliare le celle aperte
	def amplia_celle_bianche(self, ultima_cella_sbiancata:Tuple[int, int], profondita:Union[int, float]) -> None:
		#metodo che cerca tra le celle adiacenti a quella appena sbiancata, in base al parametro della tipolgoia di ricerca si ferma dopo la prima iterazione o meno
		#la prima cosa da verificare è la profondita, se 0 posso gia terminare
		if profondita <= 0:
			return
		#altrimenti psoso procedere a ricorsione con profondita -1
		#mi calcolo le celle adiacenti che sono libere
		(r_cella_bianca, c_cella_bianca) = ultima_cella_sbiancata
		celle_adiacenti_non_analizzte = [ (r,c) for (r,c) in self.calcola_adiacenti(r=r_cella_bianca, c=c_cella_bianca) if self.matrice_numeri[r][c] == Costanti.labirinto_numero_non_analizzato ]
		#le mescolo
		random.shuffle(celle_adiacenti_non_analizzte)
		#se il flag di tipo ricerca impone di bloccarsi al primo prendo solo il primo
		if self.tipologia_ricerca == Costanti.labirinto_tipologia_ricerca_ferma_prima_muro:
			celle_adiacenti_non_analizzte = celle_adiacenti_non_analizzte[:1] #prendo solo il primo elemento
		#adesso itero per tutte le celle successive e provo ad analizzarla se si puo sbiancare o no
		for (r_next, c_next) in celle_adiacenti_non_analizzte:
			#verifico se risulta ancora non analizzata
			if self.matrice_numeri[r_next][c_next] == Costanti.labirinto_numero_non_analizzato:
				#per essere accettabile le non deve toccare altre celle bianche oltre a quella di origine
				celle_bianche_adiacneti_next = [ (r,c) for (r,c) in self.calcola_adiacenti(r=r_next, c=c_next) if self.matrice_numeri[r][c] == Costanti.labirinto_numero_passaggio ]
				if len(celle_bianche_adiacneti_next) == 1:
					#allora posso sbaincarla perche l'unica bianca e quella da cui proviene
					self.matrice_numeri[r_next][c_next] = Costanti.labirinto_numero_passaggio
					#adesso vado a ricorsione
					self.amplia_celle_bianche(ultima_cella_sbiancata=(r_next, c_next), profondita=profondita-1)
				else:
					#non posso sbiancar4la perche altrimenti mi genererebbe un altro percorso possibilmente
					#devo metterci il muro
					self.matrice_numeri[r_next][c_next] = Costanti.labirinto_numero_muro

	def calcola_celle_non_analizzate_adiacneti(self, r:int, c:int) -> int:
		#metoco per contare le celle adiacenti che non sono ancora state analizzate
		celle_adiacenti_non_analizzate = [ (r_adj, c_adj) for (r_adj, c_adj) in self.calcola_adiacenti(r=r, c=c) if self.matrice_numeri[r_adj][c_adj] == Costanti.labirinto_numero_non_analizzato ]
		return len(celle_adiacenti_non_analizzate)

	def gestisci_processo_popolamento_matrice_numeri(self, profondita:Union[int, float]) -> None:
		#metodo per gestire il processo che va a popolare le altre caselle bianche del labirinto
		#per prima cosa mi calcolo le caselle bianche che hanno ancora celle non analizzate tra quelle adiacenti
		celle_bianche_possibili_continuazione_percorso = [ (r,c) for r in range(self.dim_v) for c in range(self.dim_o) if ( (self.matrice_numeri[r][c] == Costanti.labirinto_numero_passaggio) and ( self.calcola_celle_non_analizzate_adiacneti(r=r, c=c) > 0 ) ) ]
		while len(celle_bianche_possibili_continuazione_percorso) >0:
			#fino a quando ho celle espandibili, ne espando 1
			cella_scelta = random.choice(celle_bianche_possibili_continuazione_percorso)
			self.amplia_celle_bianche(ultima_cella_sbiancata=cella_scelta, profondita=profondita)
			#ricalcolo la lista nuova
			celle_bianche_possibili_continuazione_percorso = [ (r,c) for r in range(self.dim_v) for c in range(self.dim_o) if ( (self.matrice_numeri[r][c] == Costanti.labirinto_numero_passaggio) and ( self.calcola_celle_non_analizzate_adiacneti(r=r, c=c) > 0 ) ) ]
